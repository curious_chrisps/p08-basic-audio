/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"
#include "SquareOscillator.hpp"
#include "SawOscillator.hpp"
#include "TriangleOscillator.hpp"


/**
Class containing all audio processes.
*/

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
//    ========================================== PUBLIC ==================================================
public:
    /**
     Constructor
     */
    Audio();
    
    /**
     Destructor
     */
    ~Audio();
    
    /**
     Returns the audio device manager, don't keep a copy of it!
     */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    enum Waveform
    {
        Sine,
        Square,
        Saw,
        Triangle
    };
    
    void setWaveform (Waveform newWaveform);
    
    /**
     Overridden function from MidiInputCallback.
     */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    
    /**
     Overridden functions from AudioIODeviceCallback.
     */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    

    
    
//    ========================================== PRIVATE ==================================================
private:
    AudioDeviceManager audioDeviceManager;
    bool isOn;
    
    /**
     Atomic Oscillator pointer able to be pointed at any object of a class derived from Oscillator
     */
    Atomic<Oscillator*> oscPtr;
    
    /**
     Oscillator objects.
     */
    SinOscillator sinOsc;
    SquareOscillator squOsc;
    SawOscillator sawOsc;
    TriangleOscillator triOsc;
    
    
};

#endif  // AUDIO_H_INCLUDED
