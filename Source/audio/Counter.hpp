//
//  Counter.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 14/11/2017.
//

#pragma once

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class Counter   :   private Thread
{
public:
    Counter();
    ~Counter();
    
    /** Class for counter listerners to inherit. */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {};
        
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const float counterValue) = 0;
    };
    
    void setListener (Listener* newListener)
    {
        listener = newListener;
    }
    
    /** Called to reset the counter variable start the thread. */
    void start();
    
    /**
     Called to stop the thread.
     @param timeOutMilliseconds         Time that will be waited befor the thread is actually shut down.
     */
    void stop(int timeOutMilliseconds);
    
private:
    void run() override;
    
    float counter;
    Listener* listener;
};


