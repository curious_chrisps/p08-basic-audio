/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    setWaveform (Sine);
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("VMPK Output", true);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void Audio::setWaveform (Waveform newWaveform)
{
    if (newWaveform == Sine)
        oscPtr = &sinOsc;
    else if (newWaveform == Square)
        oscPtr = &squOsc;
    else if (newWaveform == Saw)
        oscPtr = &sawOsc;
    else if (newWaveform == Triangle)
        oscPtr = &triOsc;
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    if (message.isNoteOn())
    {
        oscPtr.get()->setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
        oscPtr.get()->setAmplitude(message.getFloatVelocity());
        isOn = true;
    }else if (message.isNoteOff())
    {
        isOn = false;
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    double sinVal;
    
    
    while(numSamples--)
    {
        
        sinVal = oscPtr.get()->nextSample();
        
        *outL = sinVal  * isOn;
        *outR = sinVal  * isOn;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sinOsc.setSampleRate (device->getCurrentSampleRate());
    squOsc.setSampleRate (device->getCurrentSampleRate());
    sawOsc.setSampleRate (device->getCurrentSampleRate());
   
    isOn = false;

}

void Audio::audioDeviceStopped()
{

}
