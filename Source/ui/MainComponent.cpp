/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a)
{
    setSize (500, 400);
    
    
    addAndMakeVisible(toggleThreadBtn);
    toggleThreadBtn.setButtonText("Start/Stop Thread");
    toggleThreadBtn.addListener(this);
    
    addAndMakeVisible(waveChooserBox);
    waveChooserBox.addItem("Sine", 1);
    waveChooserBox.addItem("Square", 2);
    waveChooserBox.addItem("Saw", 3);
    waveChooserBox.addItem("Triagle", 4);
    waveChooserBox.setSelectedId(1);
    waveChooserBox.addListener(this);
    
    counterYAY.setListener(this);
}

MainComponent::~MainComponent()
{
    counterYAY.setListener(nullptr);
}

void MainComponent::resized()
{
    toggleThreadBtn.setBounds(10, 10, getWidth() - 20, 25);
    waveChooserBox.setBounds(10, 40, getWidth() - 20, 25);
}



void MainComponent::buttonClicked(Button* button)
{
    if (button == &toggleThreadBtn)
    {
        if (toggleThreadBtn.getToggleState())
        {
            counterYAY.start();
        }
        else if (!toggleThreadBtn.getToggleState())
        {
            counterYAY.stop(500);
        }
    }
}

void MainComponent::counterChanged (const float counterValue)
{
    DBG(counterValue);
}

void MainComponent::comboBoxChanged(ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &waveChooserBox)
    {
        if (waveChooserBox.getSelectedId() == 1)
        {
            audio.setWaveform (Audio::Sine);
        }
        else if (waveChooserBox.getSelectedId() == 2)
        {
            audio.setWaveform (Audio::Square);
        }
        else if (waveChooserBox.getSelectedId() == 3)
        {
            audio.setWaveform (Audio::Saw);
        }
        else if (waveChooserBox.getSelectedId() == 4)
        {
            audio.setWaveform (Audio::Triangle);
        }
    }
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}


