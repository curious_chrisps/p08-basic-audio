/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "../audio/Counter.hpp"


//==============================================================================
/* This component lives inside our window, and this is where you should put all
    your controls and content. */
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Button::Listener,
                        public ComboBox::Listener,
                        public Counter::Listener
{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& a);

    /** Destructor */
    ~MainComponent();

    // GUI Function
    void resized() override;
    void buttonClicked(Button* button) override;
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    // Thread Function
     void counterChanged (const float counterValue) override;
    
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
    
private:
    Audio& audio;
    ToggleButton toggleThreadBtn;
    Counter counterYAY;
    ComboBox waveChooserBox;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
